import java.util.Scanner;

public class ex4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int budget = Integer.parseInt(scanner.nextLine());
        String season = scanner.nextLine();
        int fisher = Integer.parseInt(scanner.nextLine());
        int rent = 0;
        double discount = 0.0;
        if ("Spring".equals(season)) {
            rent = 3000;
        }else if ("Summer && Autumn".equals(season)) {
            rent = 4200;
        }else if("Winter".equals(season)) {
            rent = 2600;
        }
        if (fisher <= 6) {
            discount = rent * 0.10;
        }else if (fisher >= 7 && fisher <= 11) {
            discount = rent * 0.15;
        }else if (fisher >12) {
            discount = rent * 0.25;
        }
        double price = rent - discount;
        double moneyLeft = budget - price;
        double moneyNeed = price - budget;
        if (budget > price) {
            System.out.printf("Yes! You have %.2f leva left.", moneyLeft);
        } else if(price > budget) {
            System.out.printf("Not enough money! You need %.2f leva.", moneyNeed);
        }
    }
}
