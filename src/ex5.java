import java.util.Scanner;

public class ex5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double budget = Double.parseDouble(scanner.nextLine());
        String season = scanner.nextLine();
        String place = "";
        String type = "";
        double outlay = 0.0;
        if (budget <= 100) {
            if ("summer".equals(season)) {
                place = "Bulgaria";
                type = "Camp";
                outlay = budget * 0.30;
            }else if ("winter".equals(season)) {
                place = "Bulgaria";
                type = "Hotel";
                outlay = budget * 0.70;
            }
        }else if (budget <= 1000) {
            if ("summer".equals(season)) {
                place = "Balkans";
                type = "Camp";
                outlay = budget * 0.40;
            } else if ("winter".equals(season)) {
                place = "Balkans";
                type = "Hotel";
                outlay = budget * 0.80;
            }
        }else {
            place = "Europe";
            type = "Hotel";
            outlay = budget * 0.90;
        }
        System.out.printf("Somewhere in %s.", place);

        System.out.printf(" %s - %.2f", type, outlay);
    }
}
