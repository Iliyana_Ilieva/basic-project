import java.util.Scanner;

public class ex3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String species = scanner.nextLine();
        int number = Integer.parseInt(scanner.nextLine());
        int budget = Integer.parseInt(scanner.nextLine());
        double discount = 0.0;
        double price = 0.0 + discount;
        if ("Roses".equals(species)) {
            price = number * 5;
            if (number > 80) {
                discount = price * 0.10;
                price = price + discount;
            } else {
                discount = 0;
                price = price + discount;
            }
        } else if ("Dahia".equals(species)) {
            price = number * 3.80;
            if (number > 90) {
                discount = price * 0.15;
                price = price + discount;
            } else {
                discount = 0;
                price = price + discount;
            }
        } else if ("Tulip".equals(species)) {
            price = number * 2.80;
            if (number > 80) {
                discount = price * 0.15;
                price = price + discount;
            } else {
                discount = 0;
                price = price + discount;
            }
        } else if ("narcissus".equals(species)) {
            price = number * 3;
            if (number < 120) {
                discount = price + (price * 0.15);
                price = price + discount;
            } else {
                discount = 0;
                price = price + discount;
            }
        } else if("Gladiolus".equals(species)) {
            price = number * 3;
            if (number < 80) {
                discount = price +  (price * 0.20);
                price = price + discount;
            } else {
                discount = 0;
                price = price + discount;
            }
        }
        if (budget > price) {
            double moneyLeft = budget - price;
            System.out.printf("Hey, you have a great garden with %o %s and %.2f leva left.", number, species, moneyLeft);
        } else {
            double moneyNeed = price - budget;
            System.out.printf("Not enough money, you need %.2f leva more.", moneyNeed);
        }
    }
}