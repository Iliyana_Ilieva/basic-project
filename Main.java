import java.util.Scanner;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {}


    public static void ex1_1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int yearlyFee = Integer.parseInt(scanner.nextLine());
        double sneakersPrice = yearlyFee * 0.60;
        double outfitPrice = sneakersPrice * 0.80;
        double ballPrice = outfitPrice / 4;
        double accessoriesPrice = ballPrice / 5;
        double totalPrice = yearlyFee + sneakersPrice + outfitPrice + ballPrice + accessoriesPrice;
        System.out.printf("%.2f", totalPrice);
    }

    public static void ex1_2(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double racketPrice = Double.parseDouble(scanner.nextLine());
        int racketCount = Integer.parseInt(scanner.nextLine());
        int sneakersCount = Integer.parseInt(scanner.nextLine());
        double sneakersPrice = racketPrice / 6;
        double totalRacketPrice = racketPrice * racketCount;
        double totalSneakersPrice = sneakersCount * sneakersPrice;
        double otherEquipmentPrice = (totalRacketPrice + totalSneakersPrice) * 0.20;
        double totalPrice = totalRacketPrice + totalSneakersPrice + otherEquipmentPrice;
        int DjokovicPrice = (int) totalPrice / 8;
        int SponsorsPrice = (int) totalPrice * 7/8;
        System.out.printf("Price to be paid by Djokovic %d%n", DjokovicPrice);
        System.out.printf("Price to be paid by sponsors %d%n", SponsorsPrice);
    }

    public static void ex2_1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String firstMatch = scanner.nextLine();
        String secondMatch = scanner.nextLine();
        String thirdMatch = scanner.nextLine();
        int wins = 0;
        int losses = 0;
        int draws = 0;
    }

    public static void ex2_2(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int min = Integer.parseInt(scanner.nextLine());
        int sec = Integer.parseInt(scanner.nextLine());
        double length = Double.parseDouble(scanner.nextLine());
        int secPer100m = Integer.parseInt(scanner.nextLine());
        int quotaInSec = min * 60 + sec;
        double Time = length / 120;
        double totalTime = secPer100m * 2.5;
        double MartinTime = (length / 100) * Time - totalTime;
        if (MartinTime <= quotaInSec) {
            System.out.println("Martin Bangiev won an Olympic quota!");
            System.out.printf("His time is %.3f.", MartinTime);
        } else {
            double secc = MartinTime - quotaInSec;
            System.out.printf("No, Martin failed! He was %.3f seconds slower.", secc);
        }
    }

    public static void ex3_1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String country = scanner.nextLine();
        String apparatus = scanner.nextLine();
        double difficulty = 0.0;
        double execution = 0.0;
        switch (country) {
            case "Russia":
                switch (apparatus) {
                    case "ribbon" -> {
                        difficulty = 9.100;
                        execution = 9.400;
                    }
                    case "hoop" -> {
                        difficulty = 9.300;
                        execution = 9.800;
                    }
                    case "rope" -> {
                        difficulty = 9.600;
                        execution = 9.000;
                    }
                }
                break;
            case "Bulgaria":
                switch (apparatus) {
                    case "ribbon" -> {
                        difficulty = 9.600;
                        execution = 9.400;
                    }
                    case "hoop" -> {
                        difficulty = 9.550;
                        execution = 9.750;
                    }
                    case "rope" -> {
                        difficulty = 9.500;
                        execution = 9.400;
                    }
                };
                break;
            case "Italy":
                switch (apparatus) {
                    case "ribbon" -> {
                        difficulty = 9.200;
                        execution = 9.500;
                    }
                    case "hoop" -> {
                        difficulty = 9.450;
                        execution = 9.350;
                    }
                    case "rope" -> {
                        difficulty = 9.700;
                        execution = 9.150;
                    }
                }
                break;
        }
        double totalScore = difficulty + execution;
        double maxScore = 20.0;
        double missingPercentage = ((maxScore - totalScore) / maxScore) * 100;
        System.out.printf("The team of %s get %.3f on %s.%n", country, totalScore, apparatus);
        System.out.printf("%.2f%%%n", missingPercentage);
    }

    public static void ex3_2(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String etap = scanner.nextLine();
        String billet = scanner.nextLine();
        int billetCount = Integer.parseInt(scanner.nextLine());
        String photo = scanner.nextLine();
        double billetPrice = 0.0;
        switch (etap) {
            case "Quarter final":
                switch (billet) {
                    case "Standard" -> {
                        billetPrice = 55.50;
                    }
                    case "Premium" -> {
                        billetPrice = 105.20;
                    }
                    case "VIP" -> {
                        billetPrice = 118.90;
                    }
                }
                break;
            case "Semi final":
                switch (billet) {
                    case "Standard" -> {
                        billetPrice = 75.88;
                    }
                    case "Premium" -> {
                        billetPrice = 125.22;
                    }
                    case "VIP" -> {
                        billetPrice = 300.40;
                    }
                }
                break;
            case "Final":
                switch (billet) {
                    case "Standard" -> {
                        billetPrice = 110.10;
                    }
                    case "Premium" -> {
                        billetPrice = 160.66;
                    }
                    case "VIP" -> {
                        billetPrice = 400;
                    }
                }
                break;
        }
        double totalBilletPrice = billetPrice * billetCount;
        int photoPrice = 0;
        double discount = 0.0;
        if (photo.equals("Y")) {
            photoPrice = 40;
        }
        if (totalBilletPrice > 4000) {
            discount = totalBilletPrice * 0.25;
            photoPrice = 0;
        }else {
            discount = totalBilletPrice * 0.10;
        }
        double totalPrice = (totalBilletPrice + photoPrice) - discount;
        System.out.printf("%.2f", totalPrice);
    }

    public static void ex5_1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int visitorsCount = Integer.parseInt(scanner.nextLine());
        int backCount = 0;
        int chestCount = 0;
        int legsCount = 0;
        int absCount = 0;
        int proteinShakeCount = 0;
        int proteinBarCount = 0;
        for (int i = 0; i < visitorsCount; i++) {
            String activity = scanner.nextLine();
            switch (activity) {
                case "Back":
                    backCount++;
                    break;
                case "Chest":
                    chestCount++;
                    break;
                case "Legs":
                    legsCount++;
                    break;
                case "Abs":
                    absCount++;
                    break;
                case "Protein shake":
                    proteinShakeCount++;
                    break;
                case "Protein bar":
                    proteinBarCount++;
                    break;
            }
        }
        int workoutCount = backCount + chestCount + legsCount + absCount;
        int proteinCount = proteinShakeCount + proteinBarCount;
        double workoutPercentage = (workoutCount / (double) visitorsCount) * 100;
        double proteinPercentage = (proteinCount / (double) visitorsCount) * 100;
        System.out.printf("%d - back%n", backCount);
        System.out.printf("%d - chest%n", chestCount);
        System.out.printf("%d - legs%n", legsCount);
        System.out.printf("%d - abs%n", absCount);
        System.out.printf("%d - protein shake%n", proteinShakeCount);
        System.out.printf("%d - protein bar%n", proteinBarCount);
        System.out.printf("%.2f%% - work out%n", workoutPercentage);
        System.out.printf("%.2f%% - protein%n", proteinPercentage);
    }

    public static void ex5_2(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int tournamentsCount = Integer.parseInt(scanner.nextLine());
        int totalPoints = Integer.parseInt(scanner.nextLine());
        int pointsFromTournaments = 0;
        int winsCount = 0;
        for (int i = 0; i < tournamentsCount; i++) {
            String result = scanner.nextLine();
            switch (result) {
                case "W":
                    totalPoints += 2000;
                    pointsFromTournaments += 2000;
                    winsCount++;
                    break;
                case "F":
                    totalPoints += 1200;
                    pointsFromTournaments += 1200;
                    break;
                case "SF":
                    totalPoints += 720;
                    pointsFromTournaments += 720;
                    break;
            }
        }
        int averagePoints = pointsFromTournaments / tournamentsCount;
        double winPercentage = (winsCount / (double) tournamentsCount) * 100;
        System.out.printf("Final points: %d%n", totalPoints);
        System.out.printf("Average points: %d%n", averagePoints);
        System.out.printf("%.2f%%%n", winPercentage);
    }
}